console.log('No 1 Loop 1');

var naik = 2;

while (naik <= 20) {

    console.log(naik, 'I MIGHT LOVE CODING');
    naik+=2;
}

console.log('\n');

console.log('No 2 Loop 2');

var turun = 20;
var total =0;

while (turun > 0) {

    total += turun;
    turun-=2;

    console.log(turun, 'I Might Become A Programmer As Well');
}
